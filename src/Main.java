import java.util.Random;
public class Main {
    public static void main(String[] args) {

     //1. Cetak isi Array
        Random nilai = new Random();
        int[] array = new int[10];
        System.out.println("Berikut adalah isi array :");
        for(int i = 0; i<10;i++) {
            array[i] = nilai.nextInt(100);
            System.out.print(array[i]+" ");
        }
        System.out.println();

     //2. Cari nilai terbesar
        System.out.println();
        int nilaiTerbesar = array[0];
           for(int i = 0; i<10;i++){
               if (array[i]>nilaiTerbesar){
                   nilaiTerbesar=array[i];
               }
           }
        System.out.println("Nilai terbesar = "+nilaiTerbesar);
        System.out.println();

    //3. Hitung total dan rata-rata
        double total = 0;
        double rataRata = 0;
        for (int i=0; i<array.length;i++){
            total += array[i];
            rataRata = total/ array.length;
        }
        System.out.println("Total = "+ total);
        System.out.println("Rata-rata = "+ rataRata);
        System.out.println();

    //4. Tampilkan reserve isi array
        System.out.println("Nilai reverse isi array adalah sebagai berikut : ");
        for(int i = array.length - 1;i >= 0;i--)
        {
            System.out.print(array[i] + " ");
        }
        System.out.println();

    //5. Hitung Standar Deviasinya
        System.out.println();
        double variance = 0;
        for (int i = 0;i < array.length;i++)
        {
            variance += ((Math.pow(array[i] - rataRata,2)/ array.length));
        }
        double deviasi = Math.sqrt(variance);
        System.out.println("Nilai standar deviasi = "+deviasi);
        System.out.println();

    //6. Urutkan data array
        int urut;
        for (int i = 0; i < array.length; i++)
        {
            for (int j = i + 1; j < array.length; j++)
            {

                if (array[i] > array[j])
                {
                    urut = array[i];
                    array[i] = array[j];
                    array[j] = urut;
                }
            }
        }
        System.out.println("Nilai setelah diurutkan adalah sebagai berikut : ");
        for (int i = 0; i < array.length; i++){
            System.out.print(array[i] + " ");
        }
        System.out.println();



    }
}